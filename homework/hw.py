from scipy.stats import chi
import numpy as np
from matplotlib import pyplot as plt
import seaborn

for k in xrange(1,6):
    x = np.linspace(0,10,100)
    y = chi.pdf(x, k)
    plt.subplot(5,1,k)
    plt.plot(x, y)
    plt.title("k = {}".format(k))

plt.tight_layout()
plt.savefig("hw.png")
plt.show()
